Emacs for Windows
=================

![Emacs][Images/Emacs]
![for][Images/for]
![Windows][Images/Windows]

Looking for Vim?
================

If you are looking for bleeding-edge [Vim][Vim/Wikipedia] with decent set of
features for [Microsoft Windows][Windows/Wikipedia], then you could look into
my [Vim for Windows][].

Table of Contents
=================

-   [About](#markdown-header-about)
-   [Installation](#markdown-header-installation)
    -   [Requirements](#markdown-header-requirements)
        -   [Platforms](#markdown-header-platforms)
        -   [Architectures](#markdown-header-architectures)
    -   [Instructions](#markdown-header-instructions)
-   [Miscellaneous](#markdown-header-miscellaneous)
-   [Downloads](#markdown-header-downloads)
    -   [Featured](#markdown-header-featured)
    -   [Previous](#markdown-header-previous)

About
=====

Installation
============

Requirements
------------

### Platforms

-   Windows 2000;
-   Windows XP;
-   Windows Vista;
-   Windows 7;
-   Windows 8.

### Architectures

-   x86 (x86-32, x32, i686);
-   x64 (x86-64, amd64).

Instructions
------------

Miscellaneous
=============

If you are interested which toolchain I use to build [Emacs for Windows][] as
well as other native software for Windows in general, then I'd be glad to say
that I'm using [MinGW-w64][] (not [MinGW][]!) which is a production quality
toolchain, bringing bleeding-edge [GCC][GCC/Wikipedia] features to Windows for
both x86 and x64 architectures.

Downloads
=========

Featured
--------

| x64                                                                    | x86                                                                    | Emacs   |
|:----------------------------------------------------------------------:|:----------------------------------------------------------------------:|:-------:|
| ![Download][Images/Download]* *[Download][Downloads/Emacs/24.4.51/x64] | ![Download][Images/Download]* *[Download][Downloads/Emacs/24.4.51/x86] | 24.4.51 |

Previous
--------

| x64                                                                    | x86                                                                    | Emacs   |
|:----------------------------------------------------------------------:|:----------------------------------------------------------------------:|:-------:|
|                                                                        |                                                                        |         |

[Vim/Wikipedia]: http://en.wikipedia.org/wiki/Vim_(text_editor)

[Windows/Wikipedia]: http://en.wikipedia.org/wiki/Microsoft_Windows

[GCC/Wikipedia]: http://en.wikipedia.org/wiki/GNU_Compiler_Collection

[Emacs/Wikipedia]: http://en.wikipedia.org/wiki/Emacs

[MinGW]: http://www.mingw.org/

[MinGW-w64]: http://mingw-w64.sourceforge.net/

[Vim for Windows]:   ../../../vim-for-windows
[Emacs for Windows]: ../..

[Downloads/Emacs/24.4.51/x64]: ../../downloads/emacs-24.4.51-windows-x64.zip
[Downloads/Emacs/24.4.51/x86]: ../../downloads/emacs-24.4.51-windows-x86.zip

[Images/Emacs]:    ../../raw/master/images/400-400/emacs.png              "Emacs"
[Images/for]:      ../../raw/master/images/200-400/violet-right-arrow.png "for"
[Images/Windows]:  ../../raw/master/images/400-400/windows.png            "Windows"
[Images/Download]: ../../raw/master/images/16-16/download.png             "Download"
